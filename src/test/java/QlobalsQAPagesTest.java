import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import Pages.DatePickerSitePage;
import Pages.DemoTestingSitePage;
import PageFactory.MainPageQlobalsQA;
import Pages.ProgressBarMenuItemPages;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class QlobalsQAPagesTest {
    WebDriver driver;
    WebDriverWait wait;
    MainPageQlobalsQA mainPageQlobalsQA;
    DemoTestingSitePage demoTestingSitePage;
    DatePickerSitePage datePickerSitePage;
    ProgressBarMenuItemPages progressBarMenuItemPages;

    @BeforeMethod
    public void WebDriverInstallation() {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        options.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get("https://www.globalsqa.com/");
    }

    @AfterMethod
    public void WebDriverInstallationClose() {
        try {

        } finally {
            driver.quit();
        }
    }

    @Test(priority = 2)
    void TestersHubDemoTestingSiteStepNumberOfElements() {
        mainPageQlobalsQA = new MainPageQlobalsQA(driver);
        demoTestingSitePage = new DemoTestingSitePage(driver);
        mainPageQlobalsQA.demoTestingHubItem();
        int expected = 6;
        assertEquals(demoTestingSitePage.FirstStepColumn.size(), expected, "The elements is not equals." +
                "Please check the element size!");
        assertEquals(demoTestingSitePage.SecondStepColoumn.size(), expected, "The elements is not equals." +
                "Please check the element size!");
        assertEquals(demoTestingSitePage.ThirdStepColoumn.size(), expected, "The elements is not equals." +
                "Please check the element size!");

    }

    @Test(priority = 0)
    void DatePicker() {
        mainPageQlobalsQA = new MainPageQlobalsQA(driver);
        datePickerSitePage = new DatePickerSitePage(driver);
        mainPageQlobalsQA.datePickerItem();
        datePickerSitePage.DatePickerChecking();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        assertEquals(datePickerSitePage.expected, datePickerSitePage.actual, "The date is not true");
    }

    @Test(priority = 1)
    void ProgressBarDownloadBtn() {
        mainPageQlobalsQA = new MainPageQlobalsQA(driver);
        progressBarMenuItemPages = new ProgressBarMenuItemPages(driver);
        mainPageQlobalsQA.ProgressBarItem();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        progressBarMenuItemPages.loadingDone();
        progressBarMenuItemPages.DownloadingDone();
        assertTrue(progressBarMenuItemPages.CompleteLabel.isDisplayed(), "Loading is not done");


    }

}
