package Pages;

import PageFactory.MainPageQlobalsQA;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class DemoTestingSitePage extends MainPageQlobalsQA {
    @FindBy(xpath = "//li[text()=\"First Step\"]/following-sibling::li")
    public List<WebElement> FirstStepColumn;
    @FindBy(xpath = "//li[text()=\"Second Step\"]/following-sibling::li")
    public List<WebElement> SecondStepColoumn;
    @FindBy(xpath = "//li[text()=\"Third Step\"]/following-sibling::li")
    public List<WebElement> ThirdStepColoumn;


    public DemoTestingSitePage(WebDriver driver1) {
        super(driver1);
    }
}
