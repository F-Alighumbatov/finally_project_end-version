package Pages;


import PageFactory.MainPageQlobalsQA;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProgressBarMenuItemPages extends MainPageQlobalsQA {
    @FindBy(xpath = "//*[@id=\"post-2671\"]/div[2]/div/div/div[1]/p/iframe")
    WebElement ProgressBariFrame;
    @FindBy(id = "downloadButton")
    public WebElement ProgressBarDownload;
    @FindBy(xpath = "//div[@class='progress-label']")
    public WebElement CompleteLabel;

    public ProgressBarMenuItemPages(WebDriver driver1) {
        super(driver1);
    }

    public void loadingDone() {
        driver1.switchTo().frame(ProgressBariFrame);
        ProgressBarDownload.click();
    }

    public void DownloadingDone() {
        wait1.until(ExpectedConditions.textToBePresentInElement(CompleteLabel, "Complete!"));
    }
}
