package Pages;

import PageFactory.MainPageQlobalsQA;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

public class DatePickerSitePage extends MainPageQlobalsQA {
    public String expected;
    public String actual = "'0''8''/''2''5''/''2''0''2''1'";

    @FindBy(xpath = "//*[@id=\"post-2661\"]/div[2]/div/div/div[1]/p/iframe")
    WebElement DatePickeriFrame;

    @FindBy(className = "hasDatepicker")
    public WebElement DatePickerTextBox;

    @FindBy(xpath = "//*[@id=\"ui-datepicker-div\"]/div/a[2]")
    WebElement DatePickerNextBtn;

    @FindBy(xpath = "//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[4]/td[4]/a")
    WebElement DatePickerChooseDate;

    public DatePickerSitePage(WebDriver driver1) {
        super(driver1);
    }

    public void DatePickerChecking() {
        driver1.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver1.switchTo().frame(DatePickeriFrame);
        DatePickerTextBox.click();
        DatePickerNextBtn.click();
        DatePickerChooseDate.click();
        expected = DatePickerTextBox.getText();
        DateTimeFormatter str = DateTimeFormatter.ofPattern(DatePickerTextBox.getAttribute("value"));
        expected = str.toString();


    }
}
