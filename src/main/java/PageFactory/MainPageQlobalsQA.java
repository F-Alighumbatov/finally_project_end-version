package PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPageQlobalsQA {
   public WebDriver driver1;
   public WebDriverWait wait1;
    @FindBy(xpath = "//*[@id=\"menu-item-2822\"]/a")
    WebElement TesterHubBtn;
    @FindBy(xpath = "//*[@id=\"menu-item-2823\"]/a/span[\"Demo Testing Site\"]")
    WebElement DemoTestingSite;
    @FindBy(xpath = "//*[@id=\"menu-item-2827\"]/a/span[\"Date Picker\"]")
    WebElement DatePicker;
    @FindBy(xpath = "//*[@id=\"menu-item-2832\"]/a/span[\"Progress Bar\"]")
    WebElement ProgressBarMenuItem;

    public MainPageQlobalsQA(WebDriver driver1) {
        wait1 = new WebDriverWait(driver1, 20);
        this.driver1 = driver1;
        PageFactory.initElements(driver1, this);
    }

    public void demoTestingHubItem() {
        Actions actions = new Actions(driver1);
        actions.moveToElement(TesterHubBtn).build().perform();
        DemoTestingSite.click();
    }

    public void datePickerItem() {
        Actions actions = new Actions(driver1);
        actions.moveToElement(TesterHubBtn).build().perform();
        actions.moveToElement(DemoTestingSite).perform();
        DatePicker.click();
    }

    public void ProgressBarItem() {
        Actions actions = new Actions(driver1);
        actions.moveToElement(TesterHubBtn).build().perform();
        actions.moveToElement(DemoTestingSite).perform();
        ProgressBarMenuItem.click();
    }
}
